import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new product into the database...")
    const productsRepositery = AppDataSource.getRepository(Product)
    
    const products = await productsRepositery.find()
    console.log("Loaded products: ", products)

    const updatedProduct = await productsRepositery.findOneBy({id:2})
    console.log(updatedProduct)
    updatedProduct.price = 109
    await productsRepositery.save(updatedProduct)

}).catch(error => console.log(error))

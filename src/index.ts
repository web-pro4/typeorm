import { AppDataSource } from "./data-source"
import { User } from "./entity/User"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const usersRepositery = AppDataSource.getRepository(User)
    const user = new User()
    user.login = "user1"
    user.name = "User 1"
    user.passowrd = "Pass@1234"
    await usersRepositery.save(user)
    console.log("Saved a new user with id: " + user.id)

    console.log("Loading users from the database...")
    const users = await usersRepositery.find()
    console.log("Loaded users: ", users)


}).catch(error => console.log(error))
